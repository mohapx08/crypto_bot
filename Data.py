import requests
import pandas as pd
import datetime as dt
import csv
import asyncio
import numpy as np
from aiogram import Bot
import matplotlib.pyplot as plt
from sklearn.cluster import DBSCAN
from sklearn.preprocessing import RobustScaler
import io
import seaborn as sns
import os

class Db_crypto:
    def __init__(self, crypto_list, crypto_dict, path_file, url):
        self.crypto_list = crypto_list
        self.crypto_dict = crypto_dict
        self.path_file = path_file
        self.url = url

    def new_data(self) -> dict:
        self.crypto_dict['time'] = dt.datetime.now().timestamp()
        for i in self.crypto_list:
            self.crypto_dict[i] = round(float(requests.get(self.url + i).text), 3)
        return self.crypto_dict

    def db_write(self):
        with open(self.path_file, 'a', newline='') as f_object:
            dictwriter_object = csv.DictWriter(f_object, fieldnames=self.crypto_dict.keys(), delimiter=';')
            dictwriter_object.writerow(self.crypto_dict)
            f_object.close()

    def db_read(self):
        df = pd.read_csv(self.path_file, sep=';')
        df.iloc[:,0] = pd.to_datetime(round(df.iloc[:,0], 0), unit='s')
        return df

class Alert:

    def __init__(self, metrics_list, crypto_df, token, chat_id = 1625043687):
        self.crypto_df = crypto_df
        self.token = token
        self.chat_id = chat_id
        self.metrics_list = metrics_list

    def run_alert(self):

        def check_anomaly_quantile(df, metric, a=3, n=4):
            # функция check_anomaly_quantile предлагает алгоритм проверки значения на аномальность посредством
            # метода межквартильного размаха за период
            df['q25'] = df[metric].shift(1).rolling(n).quantile(0.25)  # скользящее значение квантиля 0.25 за период n
            df['q75'] = df[metric].shift(1).rolling(n).quantile(
                0.75)  # достаем максимальную 15-минутку из датафрейма - ту, которую будем проверять на аномальность
            df['iqr'] = df['q75'] - df['q25']
            df['up'] = df['q75'] + a * df['iqr']
            df['low'] = df['q25'] - a * df['iqr']
            df['up'] = df['up'].rolling(n, center=True).mean()
            df['low'] = df['low'].rolling(n, center=True).mean()
            if df[metric].iloc[-1] < df['low'].iloc[-2] or df[metric].iloc[-1] > df['up'].iloc[-2]:
                is_alert = 1
            else:
                is_alert = 0
            diff = 100 *(df[metric].iloc[-1] - df[metric].iloc[-2])/df[metric].iloc[-1]
            return is_alert, df, diff

        def check_DBSCAN(df):
            scaler = RobustScaler()
            scaled_data = pd.DataFrame(
                data=scaler.fit_transform(df),
                columns=df.columns)
            model = DBSCAN(eps=0.5, min_samples=5, n_jobs=-1)
            model.fit(scaled_data)
            density_outlier = np.array([1 if label == -1 else 0 for label in model.labels_])
            return density_outlier

        def send_alert(token, picture, message):
            bot = Bot(token)
            async def send_message(channel_id: int, picture, message):
                await bot.send_photo(channel_id, picture, message)
            asyncio.set_event_loop_policy(asyncio.WindowsSelectorEventLoopPolicy())
            loop = asyncio.get_event_loop()
            loop.run_until_complete(send_message(self.chat_id, picture, message))


        density_outlier = check_DBSCAN(self.crypto_df[self.metrics_list].tail(100))
        for metric in self.metrics_list:
            df_copy = self.crypto_df.copy()
            is_alert_quantile, df_quantile, diff_quantile = check_anomaly_quantile(df_copy, metric)
            if is_alert_quantile == 1 or density_outlier[-1] == 1:
                msg = '''Стоимость {metric} за {date}:\nТекущее значение = {current_value:.2f};\
                    \nПредыдущее значение = {last_value:.2f};\
                    \nОтклонение от предыдущего значения {quantile_value:.2f}%.'''.format(
                    date=df_quantile.iloc[-1,0].strftime("%d %b"),
                    last_value=df_quantile[metric].iloc[-2],
                    metric=metric,
                    current_value=df_quantile[metric].iloc[-1],
                    quantile_value=diff_quantile)
                sns.set(rc={'figure.figsize': (16, 10)})  # задаем размер графика
                plt.tight_layout()
                hour_time = df_quantile.iloc[:,0].dt.strftime("%H:%M")
                ax = sns.lineplot(x=hour_time, y=df_quantile[metric], label=metric, linewidth=2.5)
                ax = sns.lineplot(x=hour_time, y=df_quantile.up, label='up_quantile', linestyle='dashdot', linewidth=1)
                ax = sns.lineplot(x=hour_time, y=df_quantile.low, label='low_quantile', linestyle='dashdot', linewidth=1)
                for ind, label in enumerate(ax.get_xticklabels()):
                    if ind % 15 == 0:
                        label.set_visible(True)
                    else:
                        label.set_visible(False)
                ax.set_xlabel("Время", fontsize=12, loc='right')
                ax.set_ylabel(metric, fontsize=14, loc='top')
                ax.set_title(f'Количество {metric}', fontsize=16)
                ax.set(ylim=(0, None))
                sns.set_style("whitegrid")
                plot_object = io.BytesIO()
                ax.figure.savefig(plot_object)
                plot_object.seek(0)
                plot_object.name = f'Алерт: {metric}.png'
                plt.close()
                send_alert(self.token, plot_object,msg)


if __name__ == "__main__":
    default_args = {
        'url': 'https://rate.sx/',
        'path_file': 'db/crypto.csv',
        'crypto_list': ['1BTC', '1ETH', '1SOL', '1BNB'],
        'crypto_dict': {},
    }
    token = os.environ.get("report_bot_token")
    db_crypto = Db_crypto(**default_args)
    db_crypto.new_data()
    db_crypto.db_write()
    df = db_crypto.db_read()
    db_alert = Alert(default_args['crypto_list'], df, token)
    db_alert.run_alert()